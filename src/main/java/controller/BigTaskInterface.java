package controller;

import model.Text;
import model.Word;

import java.util.List;
import java.util.Map;

public interface BigTaskInterface {
    int countOfSentencesWithDublicats();

    void printSentencesInOrder();

    String exclusiveWords();

    void findAndPrint(int length);

    Text switchWords();

    void printWords();

    Text sortByVowelPercent();

    Text sortByFirstConsonant();

    Text sortByLetterCount(String c);

    Map<Word, Integer> sortByCount(List<Word> list);

    //-------------------10-----------//
    Text deleteBetween(char a, char b);

    Text deleteConsonants(int length);

    Text sortByLetterCountDesc(char c);

    String maxPolindrom();

    Text deleteNextFirstLetter();

    Text deletePreviousLastLetter();

    Text replaceInSentence(int number, String sub);


}
