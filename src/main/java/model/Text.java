package model;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Text {
    private String it;

    public Text(String it) {
        this.it = it;
    }

    public Text() {

    }

    public String getIt() {
        return it;
    }

    public void setIt(String it) {
        this.it = it;
    }

    public Sentence[] toSentences() {
        Pattern patt = Pattern.compile("(?:[.!? ]|^)([A-Z][^.!?\n]*[.!?])(?= |[A-Z]|$)");
        Matcher matcher = patt.matcher(it);
        List<String> matchers = new ArrayList<>();
        if (matcher.find()) {
            matchers.add(matcher.group());
            while (matcher.find()) {
                matchers.add(matcher.group());
            }
        }
//        String [] arr = it.split("[?.!]");
        Sentence[] sentences = new Sentence[matchers.size()];
        for (int i = 0; i < sentences.length; i++) {
            sentences[i] = new Sentence();
            sentences[i].setIt(matchers.get(i));
        }
        return sentences;
    }

    public Word[] toWordsArray() {
        String[] arr = it.split(" ");
        Word[] words = new Word[arr.length];
        for (int i = 0; i < words.length; i++) {
            words[i] = new Word(arr[i]);
        }
        return words;
    }

    public Integer enterCount(Word word) {
        Word[] words = toWordsArray();
        Integer number = 0;
        for (int i = 0; i < words.length; i++) {
            if (words[i].equals(word)) {
                number++;
            }
        }
        return number;
    }

    @Override
    public String toString() {
        return it;
    }
}

